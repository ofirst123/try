<?php

use yii\db\Migration;

/**
 * Class m180621_080941_user_table
 */
class m180621_080941_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
                    $this->createTable('user', [
                    'id' => $this->primaryKey(),
                    'name'=> $this->string(),
                    'auth_key' => $this->string(),
                    'username' => $this->string()->unique(),
                    'password' => $this->string(),

                ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180621_080941_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_080941_user_table cannot be reverted.\n";

        return false;
    }
    */
}
