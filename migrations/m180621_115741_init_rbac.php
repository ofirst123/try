<?php

use yii\db\Migration;

/**
 * Class m180621_115741_init_rbac
 */
class m180621_115741_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
                         $auth = Yii::$app->authManager;



        // add "createUser" permission
        $createUser = $auth->createPermission('createUser');
        $createUser->description = 'Create a user';
        $auth->add($createUser);

        // add "updateUser" permission
        $updateUser = $auth->createPermission('updateUser');
        $updateUser->description = 'Update user';
        $auth->add($updateUser);

        // add "deleteUser" permission
        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'Delete user';
        $auth->add($deleteUser);
        
        // add "createPost" permission
        $createPost = $auth->createPermission('createPost');
        $createPost->description = 'Create a post';
        $auth->add($createPost);

        // add "updatePost" permission
        $updatePost = $auth->createPermission('updatePost');
        $updatePost->description = 'Update post';
        $auth->add($updatePost);

        // add "updateOwnPost" permission
        $updateOwnPost = $auth->createPermission('updateOwnPost');
        $updateOwnPost->description = 'Update Own Post';

         $rule = new \app\rbac\AuthorRule;
         $auth->add($rule);

        $updateOwnPost->ruleName = $rule->name;                 
        $auth->add($updateOwnPost); 
        

         // add "publishPost" permission
        $publishPost = $auth->createPermission('publishPost');
        $publishPost->description = 'Publish post';
        $auth->add($publishPost);

        $rule2 = new \app\rbac\AuthorRule;
        $auth->add($rule2);

        $publishPost->ruleName = $rule->name;                 
        $auth->add($publishPost); 

         // add "deletePost" permission
        $deletePost = $auth->createPermission('deletePost');
        $deletePost->description = 'Delete post';
        $auth->add($deletePost);

         // add "viewPostList" permission
        $viewPostList = $auth->createPermission('viewPostList');
        $viewPostList->description = 'view Post List';
        $auth->add($viewPostList);

         // add "viewPublishPostList" permission
        $viewPublishPostList = $auth->createPermission('viewPublishPostList');
        $viewPublishPostList->description = 'view Publish Post List';
        $auth->add($viewPublishPostList);

        // add "author" role and give this role the "createPost" permission
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createPost);
        $auth->addChild($author, $updateOwnPost);
        $auth->addChild($author, $viewPostList);

         // add "author" role and give this role the "createPost" permission
        $editor = $auth->createRole('editor');
        $auth->add($editor);
        $auth->addChild($editor, $updatePost);
        $auth->addChild($editor, $publishPost);
        $auth->addChild($editor, $deletePost);
        $auth->addChild($editor, $author);

        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $createUser);
        $auth->addChild($admin, $updateUser);
        $auth->addChild($admin, $deleteUser);
        $auth->addChild($admin, $editor);




        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($editor, 3);
        $auth->assign($author, 4);
        $auth->assign($admin, 2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
             $auth = Yii::$app->authManager;

            $auth->removeAll();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_115741_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
