<?php

use yii\db\Migration;

/**
 * Class m180621_082549_post_table
 */
class m180621_082549_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
               $this->createTable('post', [
           'id' => $this->primaryKey(),
           'title' => $this->string(),

           'body' => $this->text(),
           'author_id' => $this->integer(),
           'status' => $this->string(),
           'category_id' => $this->integer(),
           'created_at' => $this->timestamp(),
           'updated_at' => $this->timestamp(),
           'created_by' => $this->integer(),
           'updated_by' => $this->integer(),
       ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180621_082549_post_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_082549_post_table cannot be reverted.\n";

        return false;
    }
    */
}
